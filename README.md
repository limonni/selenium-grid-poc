# Selenium on Kubernetes

This repository contains the manifests related to the installlation and configuration of Selenium Grid.
At the root of the project there are the minifests related to the configuration of Argocd.
Everything under `selenium/` is related to Selenium Grid, and it's not meant to be used via `kubectl`, but instead to be automatically applied by Argocd. 

## Requirements
- Argocd installed on the cluster (already available for the AMS EKS cluster)
- Access to the AMS EKS cluster
- kubectl
- kubeseal

## Configure Argocd

In order to configure Argocd you need to run the following commands:
kubectl apply -f argocd-config.yml
kubectl apply -f argocd-repos.yml

## Bitbucket authentication details encrypted via sealed secrets
At the root of the project the file named `argocd-repos.yml` contains the authentication details used by Argocd to interact with Bitbucket. The credentials have been encrypted using sealed secrets [(bitnami sealed secrets)](https://github.com/bitnami-labs/sealed-secrets).
In case you need to change the credentials, you should re-encrypt them. You can achieve this following these steps:
- Create a file `/tmp/argocd-repos.yml` holding the new credentials in clear text:
    ```
    apiVersion: v1
    kind: Secret
    metadata:
      name: private-repo
      namespace: argocd
      labels:
        argocd.argoproj.io/secret-type: repository
    stringData:
      type: git
      url: https://citnet.tech.ec.europa.eu/CITnet/stash/scm/ams/ams-selenium-grid.git
      password: ***
      username: ***
    ```
- Encrypt the file:
    kubeseal -o yaml </tmp/argocd-repos.yml > argocd-repos.yml
- Create the secret in kubernetes:
    kubectl apply -f argocd-repos.yml
- Commit and push the encrypted file
